#!/bin/bash
echo
echo "### Starting configuration of MariaDB ###"
echo

#starts with parameters for database name and root password set in mysql-template.sh
echo "mysql_database = ${MARIADB_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"

set -e

sed -i "s|ProtectHome=.*$|ProtectHome=false|g" /lib/systemd/system/mariadb.service
sed -i "s|ProtectHome=.*$|ProtectHome=false|g" /lib/systemd/system/mariadb@.service
systemctl daemon-reload

# Get config
#	DATADIR="$("$@" --verbose --help --innodb-read-only 2>/dev/null | awk '$1 == "datadir" { print $2; exit }')"
	if [ ! -d "$DATADIR/mysql" ]; then
		if [ -z "$MARIADB_ROOT_PASSWORD" -a -z "$MARIADB_ALLOW_EMPTY_PASSWORD" ]; then
			echo >&2 'error: database is uninitialized and MARIADB_ROOT_PASSWORD not set'
			echo >&2 '  Did you forget to add -e MARIADB_ROOT_PASSWORD=... ?'
			exit 1
		fi
  fi

    rm -fR $DATADIR
    mkdir -p "$DATADIR"
		chown -R mysql:mysql "$DATADIR"

		echo 'Initializing database'
		mysql_install_db --user=mysql --datadir="$DATADIR" --verbose
		echo 'Database initialized'

		sed -i "s|^datadir.*|datadir         = $DATADIR|" "/etc/mysql/my.cnf"
		
		# deactivate mysqld_safe should not be used with systemend
		sed -i '/\[mysqld_safe\]/,/^$/d' "/etc/mysql/my.cnf"
		

		$OPENSTACK_START
		mysql=( mysql --protocol=socket -uroot )

		for i in {30..0}; do
			if echo 'SELECT 1' | "${mysql[@]}" &> /dev/null; then
				break
			fi
			echo 'MySQL init process in progress...'
			sleep 1
		done

		if [ "$i" = 0 ]; then
			echo >&2 'MySQL init process failed.'
			exit 1
		fi

		"${mysql[@]}" <<-EOSQL
			-- What's done in this file shouldn't be replicated
			--  or products like mysql-fabric won't work
			SET @@SESSION.SQL_LOG_BIN=0;
			DELETE FROM mysql.user ;
			CREATE USER '${MARIADB_USER}'@'%' IDENTIFIED BY '${MARIADB_ROOT_PASSWORD}' ;
			GRANT ALL ON *.* TO '${MARIADB_USER}'@'%' WITH GRANT OPTION ;
			DROP DATABASE IF EXISTS test ;
			FLUSH PRIVILEGES ;
		EOSQL

		if [ ! -z "$MARIADB_ROOT_PASSWORD" ]; then
			mysql+=( -u "${MARIADB_USER}" -p"${MARIADB_ROOT_PASSWORD}" )
		fi

		if [ "$MARIADB_DATABASE" ]; then
			echo "CREATE DATABASE IF NOT EXISTS \`$MARIADB_DATABASE\` ;" | "${mysql[@]}"
			mysql+=( "$MARIADB_DATABASE" )
		fi

		if [ "$MARIADB_USER" -a "$MARIADB_PASSWORD" ]; then
			echo "CREATE USER '$MARIADB_USER'@'%' IDENTIFIED BY '$MARIADB_PASSWORD' ;" | "${mysql[@]}"

			if [ "$MARIADB_DATABASE" ]; then
				echo "GRANT ALL ON \`$MARIADB_DATABASE\`.* TO '$MARIADB_USER'@'%' ;" | "${mysql[@]}"
			fi

			echo 'FLUSH PRIVILEGES ;' | "${mysql[@]}"
		fi

		
		$OPENSTACK_STOP
		
		# get access from other ips
		sed -Ei 's/^(bind-address|log)/#&/' /etc/mysql/my.cnf
		
		echo
		echo 'MySQL init process done. Ready for start up.'
		echo

	chown -R mysql:mysql "$DATADIR"
