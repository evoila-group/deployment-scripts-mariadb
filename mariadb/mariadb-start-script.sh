#!/bin/bash

export REPOSITORY_MARIADB="https://bitbucket.org/meshstack/deployment-scripts-mariadb/raw/HEAD/mariadb"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_MARIADB/mariadb-template.sh --no-cache
chmod +x mariadb-template.sh
./mariadb-template.sh -d evoila -p evoila -e openstack -l 172.24.102.12 -m 5002
