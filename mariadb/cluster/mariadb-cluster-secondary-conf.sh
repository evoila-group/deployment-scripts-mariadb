#!/bin/bash
echo
echo "### Starting Cluster Configuration of MariaDB ###"
echo

monit unmonitor mysqld
monit summary

echo "Reconfiguring the password for debian.cnf and resetting it in database"
sed -i "s|^password.*|password = joKh1KpYWwy8aOmN|" "/etc/mysql/debian.cnf"
mysql -u ${MARIADB_USER} -p${MARIADB_PASSWORD} -e "GRANT ALL PRIVILEGES ON *.* TO 'debian-sys-maint'@'localhost' IDENTIFIED BY 'joKh1KpYWwy8aOmN'";

echo "Stopping running Mysql Instance"
$OPENSTACK_STOP

echo "Writing cluster.cnf for initial cluster setup"
echo "[mysqld]
query_cache_size=0
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
query_cache_type=0
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_provider=/usr/lib/galera/libgalera_smm.so

#wsrep_provider_options="gcache.size=32G"
# Galera Cluster Configuration
wsrep_cluster_name=\"testcluster\"
wsrep_cluster_address=\"gcomm://${CLUSTER_IPs}\"

# Galera Synchronization Configuration
wsrep_sst_method=rsync

#wsrepsstauth=user:pass
# Galera Node Configuration
wsrep_node_address=\"${NODE_IP}\"
wsrep_node_name=\"node1\"" > /etc/mysql/conf.d/cluster.cnf

echo "Configuration of wsrep_on"
sed -i "s|^#wsrep_on=ON.*|wsrep_on=ON|" "/etc/mysql/my.cnf"

sleep 15
echo "Starting following Cluster nodes"
systemctl start mysql

echo "Result for Cluster size"
mysql -u ${MARIADB_USER} -p${MARIADB_PASSWORD} -e 'SELECT VARIABLE_VALUE as "cluster size" FROM INFORMATION_SCHEMA.GLOBAL_STATUS WHERE VARIABLE_NAME="wsrep_cluster_size"'


# adding file for the set of scripts for checking if cluster is configured
mkdir -p $CHECK_PATH/
chmod 700 -R  $CHECK_PATH/
echo "If this file exists, initialy a cluster was configured." > $CHECK_PATH/secondary-server

# activates monitoring of mariadb instance
# monit monitor mysqld


## Generate SystemD Script for MariaDV Exporter
cd /etc/systemd/system/
echo "[Unit]" > mysqld_exporter.service
echo "Description=MySQL/MariaDB exporter" >> mysqld_exporter.service
echo "After=local-fs.target network-online.target network.target" >> mysqld_exporter.service
echo "Wants=local-fs.target network-online.target network.target" >> mysqld_exporter.service
echo "" >> mysqld_exporter.service
echo "[Service]" >> mysqld_exporter.service
echo "Environment=DATA_SOURCE_NAME=$MARIADB_USER:$MARIADB_PASSWORD@(localhost:3306)/" >> mysqld_exporter.service
echo "ExecStart=/opt/mysqld_exporter" >> mysqld_exporter.service
echo "Type=simple" >> mysqld_exporter.service
echo "" >> mysqld_exporter.service
echo "[Install]" >> mysqld_exporter.service
echo "WantedBy=multi-user.target" >> mysqld_exporter.service

chmod 755 /etc/systemd/system/mysqld_exporter.service
systemctl daemon-reload
systemctl enable mysqld_exporter.service
systemctl start mysqld_exporter.service