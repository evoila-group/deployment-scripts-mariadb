#!/bin/bash

echo
echo "### Starting installation of MariaDB ###"
echo

#starts with parameters for database name and root password set in mariadb-template.sh
echo
echo "mysql_database = ${MARIADB_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mariadb = ${REPOSITORY_MARIADB}"
echo "check_path = ${CHECK_PATH}"
echo "mariadb_major = ${MARIADB_MAJOR}"
echo "mariadb_version = ${MARIADB_VERSION}"
echo

# adds userdirectory for user mysql
mkdir -p /home/mysql
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
groupadd -r mysql && useradd -r -g mysql mysql
chown -R mysql:mysql /home/mysql
chmod 760 -R /home/mysql

echo "Adding key to keyserver"
apt-get update
apt-get install software-properties-common -y
apt-get install python-software-properties -y


apt-get install software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8

echo "Adding Repository to apt preferences"
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirrors.n-ix.net/mariadb/repo/10.1/ubuntu xenial main'

export DEBIAN_FRONTEND=noninteractive
debconf-set-selections <<< "mariadb-server-${MARIADB_MAJOR} mysql-server/root_password password 'unused'"
debconf-set-selections <<< "mariadb-server-${MARIADB_MAJOR} mysql-server/root_password_again password 'unused'"

apt-get update && apt-get install -y -o Dpkg::Options::="--force-confold" mariadb-server

rm -rf /var/lib/apt/lists/*
rm -rf /var/lib/mysql
rm -rf ${DATADIR} && mkdir -p ${DATADIR}

# install Prometheus Node Exporter
sudo su
apt-get update && apt-get install -y prometheus-node-exporter

# install MariaDB Exporter
cd /home/ubuntu
wget https://github.com/prometheus/mysqld_exporter/releases/download/v0.10.0/mysqld_exporter-0.10.0.linux-amd64.tar.gz
tar -zxvf mysqld_exporter-0.10.0.linux-amd64.tar.gz
cp mysqld_exporter-0.10.0.linux-amd64/mysqld_exporter /opt 

# comment out a few problematic configuration values
# don't reverse lookup hostnames, they are usually another container
    echo -e 'skip-host-cache\nlog-error      = /var/log/mysql/error.log' | awk '{ print } $1 == "[mysqld]" && c == 0 { c = 1; system("cat") }' /etc/mysql/my.cnf > /tmp/my.cnf \
    && mv /tmp/my.cnf /etc/mysql/my.cnf

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_STOP
fi
